from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from .models import AutomobileVO, Technician, Appointment
from django.views.decorators.http import require_http_methods
import json
from datetime import datetime

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id",
    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "id",
        "employee_id",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "customer",
        "vin",
        "id",
        "technician"
    ]
    encoders = {
        "technician":TechnicianDetailEncoder(),
    }

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "customer",
        "vin",
        "id",
        "technician",
    ]
    encoders = {
        "technician":TechnicianDetailEncoder(),
    }


require_http_methods({"GET", "POST"})
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )

    else:
        content = json.loads(request.body)
        technicians = Technician.objects.all()
        for technician in technicians:
            print(technician.id)
            if technician.employee_id == content["employee_id"]:
                return JsonResponse(
                    {"message": "invalid technician"}
                    )
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )

require_http_methods({"GET", "DELETE"})
def api_show_technicians(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(technician, encoder=TechnicianDetailEncoder, safe=False)
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})


require_http_methods({"GET", "POST"})
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({'appointments': appointments}, encoder=AppointmentListEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            technician_id = content.pop('technician')
            content['date_time'] = datetime.fromisoformat(content['date_time'])
            technician = Technician.objects.get(id=technician_id)
            appointment = Appointment.objects.update_or_create(technician=technician, **content)
            return JsonResponse(appointment,encoder=AppointmentDetailEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse({"error": "Technician does not exist"}, status=404)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=500)

require_http_methods({"GET", "DELETE"})
def api_show_appointments(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            {"appointments":appointment},
            encoder = AppointmentDetailEncoder,
            safe = False,
        )
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})


require_http_methods({"PUT"})
def api_canceled_appointments(request,pk):
    if request.method =="PUT":
        canceled_appointment = Appointment.objects.get(id=pk)
        canceled_appointment.canceled()
        return JsonResponse (
            canceled_appointment,
            encoder=AppointmentListEncoder,
            safe=False

        )



require_http_methods({"PUT"})
def api_finished_appointments(request,pk):
    if request.method =="PUT":
        finished_appointment = Appointment.objects.get(id=pk)
        finished_appointment.finished()
        return JsonResponse (
            finished_appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


require_http_methods({"GET"})
def api_get_automobileVOs(request):
    if request.method =="GET":
        automobileVOs=AutomobileVO.objects.all()
        return JsonResponse (
            {"AutomobileVOs": automobileVOs},
            encoder=AutomobileVOEncoder,
            safe=False,
        )
