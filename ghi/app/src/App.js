import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import InventoryList from './InventoryList';
import InventoryForm from './InventoryForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './VehicleModelsList';
import VehicleModelsForm from './VehicleModelsForm';
import AddSalespersonForm from './AddSalespersonForm';
import SalespeopleList from './SalespeopleList';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AddCustomerForm from './AddCustomerForm';
import CustomersList from './CustomerList';
import AddSaleForm from './AddSaleForm';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory';
import ServiceHistory from './ServiceHistory';

function App() {
  return (
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="inventory" element={<InventoryList />} />
            <Route path="manufacturers" element={<ManufacturerList />} />
            <Route path="create-inventory" element={<InventoryForm />} />
            <Route path="/create-manufacturer" element={<ManufacturerForm />} />
            <Route path="vehicle-models" element={<ModelList />} />
            <Route path="create-vehicle-model" element={<VehicleModelsForm />} />
            <Route path="add-salesperson" element={<AddSalespersonForm />} />
            <Route path="salespeople" element={<SalespeopleList />} />
            <Route path="/create-technician" element={<TechnicianForm />} />
            <Route path="/technicians" element={<TechniciansList/>} />
            <Route path="/create-appointment" element={<AppointmentForm />} />
            <Route path="/appointment" element={<AppointmentList/>} />
            <Route path="add-customer" element={<AddCustomerForm />} />
            <Route path="customers" element={<CustomersList />} />
            <Route path="add-sale" element={<AddSaleForm />} />
            <Route path="sales" element={<SalesList />} />
            <Route path="salesperson-history" element={<SalespersonHistory />} />
            <Route path="/service-history" element={<ServiceHistory />} />
          </Routes>
        </div>
      </BrowserRouter>
    );
  }

export default App;
