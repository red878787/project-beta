import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-manufacturer">Create Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/vehicle-models">Vehicle Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-vehicle-model">Create Vehicle Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/inventory">Inventory</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-inventory">Create Inventory</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/add-salesperson">Add a Salesperson</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/create-technician">Create a Technician</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/technicians">Technicians</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/create-appointment">Schedule an Appointment</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/appointment">List of Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/add-customer">Add a Customer</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/add-sale">Record a new sale</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/sales">Sales List</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/salesperson-history">Salesperson History</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className="nav-link" to="/service-history">Service History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
