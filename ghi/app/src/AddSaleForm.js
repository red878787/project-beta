import React, { useState, useEffect } from 'react';

function AddSaleForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [selectedAutomobile, setSelectedAutomobile] = useState('');
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [selectedCustomer, setSelectedCustomer] = useState('');
    const [price, setPrice] = useState('');

    useEffect(() => {
        // Fetch the initial data for the drop-down menus
        fetch('http://localhost:8100/api/automobiles/')
        .then(response => response.json())
        .then(data => {
            const unsoldAutos = data.autos.filter(automobile => !automobile.sold);
            setAutomobiles(unsoldAutos);
        });

        fetch('http://localhost:8090/api/salespeople/')
            .then(response => response.json())
            .then(data => setSalespeople(data.salespeople));

        fetch('http://localhost:8090/api/customers/')
            .then(response => response.json())
            .then(data => setCustomers(data.customers));
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();

        let parsedPrice = parseFloat(price);
        if (isNaN(parsedPrice)) {
            console.log('Price should be a number');
            return;
        }

        const data = {
            automobile: selectedAutomobile,
            salesperson: selectedSalesperson,
            customer: selectedCustomer,
            price: parseFloat(price),
        };

        const url = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if(response.ok) {
            // After the sale is successful, update the automobile's sold status
            const updateUrl = `http://localhost:8100/api/automobiles/${selectedAutomobile}/`;
            const updateData = {
                sold: true,
            };
            const updateConfig = {
                method: "PUT",
                body: JSON.stringify(updateData),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const updateResponse = await fetch(updateUrl, updateConfig);
            if(updateResponse.ok) {
                // Reset the form fields
                setSelectedAutomobile('');
                setSelectedSalesperson('');
                setSelectedCustomer('');
                setPrice('');
                // Refresh the list of unsold automobiles
                fetch('http://localhost:8100/api/automobiles/')
                    .then(response => response.json())
                    .then(data => {
                        const unsoldAutos = data.autos.filter(automobile => !automobile.sold);
                        setAutomobiles(unsoldAutos);
                    });
            } else {
                console.log('Error updating automobile sold status', updateResponse.status);
            }
        } else {
            console.log('Error', response.status);
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="form-floating mb-3">
                            <select onChange={e => setSelectedAutomobile(e.target.value)} value={selectedAutomobile} required name="automobile" id="automobile" className="form-control">
                                <option value="" disabled>Select...</option>
                                {automobiles.map(automobile => (
                                    <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                ))}
                            </select>
                            <label htmlFor="automobile">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={e => setSelectedSalesperson(e.target.value)} value={selectedSalesperson} required name="salesperson" id="salesperson" className="form-control">
                                <option value="" disabled>Select...</option>
                                {salespeople.map(salesperson => (
                                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                ))}
                            </select>
                            <label htmlFor="salesperson">Salesperson</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={e => setSelectedCustomer(e.target.value)} value={selectedCustomer} required name="customer" id="customer" className="form-control">
                                <option value="" disabled>Select...</option>
                                {customers.map(customer => (
                                    <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                ))}
                            </select>
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={e => setPrice(e.target.value)} value={price} placeholder="Sale Price" required type="number" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Sale Price</label>
                        </div>
                        <button className="btn btn-primary">Record Sale</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AddSaleForm;
