import React, { useEffect, useState } from 'react';

function CustomersList() {
    const [customers, setCustomers] = useState([]);

    useEffect(() => {
        fetchCustomers();
    }, []);

    const fetchCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
        const { customers } = await response.json();
        setCustomers(customers);
        }
    };

    return (
        <>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Phone Number</th>
            </tr>
            </thead>
            <tbody>
            {customers.map(customer => (
                <tr key={customer.phone_number}>
                <td>{customer.first_name} {customer.last_name}</td>
                <td>{customer.address}</td>
                <td>{customer.phone_number}</td>
                </tr>
            ))}
            </tbody>
        </table>
        </>
    );
    }

    export default CustomersList;
