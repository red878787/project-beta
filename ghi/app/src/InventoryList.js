import { useEffect, useState } from "react";

function InventoryList() {
    const [automobiles, setInventoryList] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    const [editingVin, setEditingVin] = useState(null);
    const [updateForm, setUpdateForm] = useState({
        color: "",
        year: "",
        sold: ""
    });
    const [selectedAutoDetails, setSelectedAutoDetails] = useState(null); // Renamed variable


    async function getInventoryList() {
      const url = 'http://localhost:8100/api/automobiles/';

      try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setInventoryList(data.autos);
        }
        } catch(e) {
            console.error(e)
        }
    }
    async function selectedAutomobile(vin) {
        const url = `http://localhost:8100/api/automobiles/${vin}/`;
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setSelectedAutoDetails(data);
            }
        } catch(e) {
            console.error(e)
        }
    }
    async function deleteAutomobile(vin) {
        const url = `http://localhost:8100/api/automobiles/${vin}/`;

        try {
          const response = await fetch(url, { method: 'DELETE' });
          if (response.ok) {
              // Remove the deleted automobile from the state
              setInventoryList(automobiles.filter(automobile => automobile.vin !== vin));
          }
          } catch(e) {
              console.error(e)
          }
      }
      async function updateAutomobile(vin) {
        const url = `http://localhost:8100/api/automobiles/${vin}/`;

        try {
          const response = await fetch(url, {
              method: 'PUT',
              headers: {
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify(updateForm),
          });
          if (response.ok) {
              const updatedAutomobile = await response.json();
              setInventoryList(automobiles.map(automobile =>
                  automobile.vin === vin ? updatedAutomobile : automobile
              ));
              setEditingVin(null);
          }
          } catch(e) {
              console.error(e)
          }
      }

      function startEditing(vin) {
          const automobile = automobiles.find(automobile => automobile.vin === vin);
          setEditingVin(vin);
          setUpdateForm({
              color: automobile.color,
              year: automobile.year,
              sold: automobile.sold,
          });
      }


  useEffect(() => {
    getInventoryList();
  }, []);

  const filteredAutomobiles = automobiles.filter(automobile =>
    automobile.vin.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div>
    <input
      type="text"
      placeholder="Search by VIN"
      value={searchTerm}
      onChange={e => setSearchTerm(e.target.value)}
    />
    <table className="table table-striped">
        <thead>
            <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
            </tr>
        </thead>
        <tbody>
            {filteredAutomobiles.map(automobile => (
                <tr key={automobile.vin}>
                    <td>{automobile.vin}</td>
                    <td>{automobile.color}</td>
                    <td>{automobile.year}</td>
                    <td>{automobile.model && automobile.model.name}</td>
                    <td>{automobile.model && automobile.model.manufacturer && automobile.model.manufacturer.name}</td>
                    <td>{automobile.sold ? 'Yes' : 'No'}</td>
                    <td><button onClick={() => selectedAutomobile(automobile.vin)}>Details</button></td>
                    <td><button onClick={() => startEditing(automobile.vin)}>Update</button></td>
                    <td><button onClick={() => deleteAutomobile(automobile.vin)}>
                        Delete
                    </button></td>
        </tr>
    ))}
</tbody>
    </table>
    {editingVin && (
        <form onSubmit={(e) => {
          e.preventDefault();
          updateAutomobile(editingVin);
        }}>
          <h3>Update Automobile</h3>
          <label>
            Color:
            <input type="text" value={updateForm.color} onChange={e => setUpdateForm({ ...updateForm, color: e.target.value })} />
          </label>
          <label>
            Year:
            <input type="number" value={updateForm.year} onChange={e => setUpdateForm({ ...updateForm, year: Number(e.target.value) })} />
          </label>
          <label>
            Sold:
            <select value={updateForm.sold ? 'Yes' : 'No'} onChange={e => setUpdateForm({ ...updateForm, sold: e.target.value === 'Yes' })}>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </label>
          <button type="submit">Save Changes</button>
          <button type="button" onClick={() => setEditingVin(null)}>Cancel</button>
        </form>
        )}
    {selectedAutoDetails && (
        <div>
            <h2>Automobile Details</h2>
            <p>VIN: {selectedAutoDetails.vin}</p>
            <p>Color: {selectedAutoDetails.color}</p>
            <p>Year: {selectedAutoDetails.year}</p>
            <p>Model: {selectedAutoDetails.model && selectedAutoDetails.model.name}</p>
            <p>Manufacturer: {selectedAutoDetails.model && selectedAutoDetails.model.manufacturer && selectedAutoDetails.model.manufacturer.name}</p>
            <p>Sold: {selectedAutoDetails.sold ? 'Yes' : 'No'}</p>
            <img src={selectedAutoDetails.model && selectedAutoDetails.model.picture_url} alt={selectedAutoDetails.model && selectedAutoDetails.model.name} />
        </div>
    )}
    </div>
  );
};

export default InventoryList;
