import React, { useState, useEffect } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    async function fetchSalespeople() {
      const url = 'http://localhost:8090/api/salespeople/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      }
    }

    fetchSalespeople();
  }, []);

  return (
    <>
      <h1>Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map(salesperson => (
            <tr key={salesperson.employee_id}>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
              <td>{salesperson.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
  }

  export default SalespeopleList;
