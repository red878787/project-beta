import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function ModelList() {
    const [models, setModels] = useState([]);
    const [selectedModel, setSelectedModel] = useState(null);
    const [isUpdating, setIsUpdating] = useState(false);
    const [updatedModelName, setUpdatedModelName] = useState('');
    const [updatedModelPictureUrl, setUpdatedModelPictureUrl] = useState('');

    async function getModels() {
      const url = 'http://localhost:8100/api/models/';

      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        }
      } catch(e) {
        console.error(e)
      }
    }
    async function deleteModel(model) {
      const url = `http://localhost:8100${model.href}`;

      const fetchConfig = {
        method: "DELETE",
      };

      const response = await fetch(url, fetchConfig);
      if(response.ok) {
        getModels();
      }
    }

    async function updateModel() {
      const url = `http://localhost:8100${selectedModel.href}`;

      const data = {
        name: updatedModelName,
        picture_url: updatedModelPictureUrl
      };

      const fetchConfig = {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(url, fetchConfig);
      if(response.ok) {
        setIsUpdating(false);
        setUpdatedModelName('');
        setUpdatedModelPictureUrl('');
        getModels();
      }
    }

    useEffect(() => {
      getModels();
    }, []);

    const handleDetailsClick = (model) => {
      setSelectedModel(model);
      setIsUpdating(false);
    };

    const handleUpdateClick = (model) => {
      setSelectedModel(model);
      setIsUpdating(true);
      setUpdatedModelName(model.name);
      setUpdatedModelPictureUrl(model.picture_url);
    };

    return (
      <div>
        <Link to="/create-vehicle-model">Create a new vehicle model</Link>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Model Name</th>
              <th>Manufacturer</th>
              <th>Image</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {models.map(model => {
              return (
                <tr key={model.id}>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td><img src={model.picture_url} alt={model.name} style={{ maxWidth: "320px", maxHeight: "172px" }} /></td>
                  <td>
                    <button onClick={() => handleDetailsClick(model)}>Details</button>
                    <button onClick={() => handleUpdateClick(model)}>Update</button>
                  </td>
                  <td><button onClick={() => deleteModel(model)}>Delete</button></td>
                </tr>
              )
            })}
          </tbody>
        </table>

        {selectedModel && (
          <div>
            {!isUpdating ? (
              <>
                <h2>Details for {selectedModel.name}</h2>
                <p>ID: {selectedModel.id}</p>
                <p>Manufacturer: {selectedModel.manufacturer.name}</p>
                <img src={selectedModel.picture_url} alt={selectedModel.name} style={{ maxWidth: "320px", maxHeight: "172px" }} />
              </>
            ) : (
              <div>
                <h2>Update {selectedModel.name}</h2>
                <form onSubmit={updateModel}>
                  <label>
                    Name:
                    <input type="text" value={updatedModelName} onChange={(e) => setUpdatedModelName(e.target.value)} />
                  </label>
                  <label>
                    Picture URL:
                    <input type="text" value={updatedModelPictureUrl} onChange={(e) => setUpdatedModelPictureUrl(e.target.value)} />
                  </label>
                  <button type="submit">Submit Update</button>
                </form>
              </div>
            )}
          </div>
        )}
      </div>
    );
  };

  export default ModelList;
