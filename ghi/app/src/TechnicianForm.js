import React, { useEffect, useState } from "react";

function TechnicianForm() {
    const [first_name, setFirst_name] = useState('');
    const [last_name, setLast_name] = useState('');
    const [employee_id, setEmployeeid] = useState('');

    const handleFirst_nameChange = (e) => {
        setFirst_name(e.target.value)
    }

    const handleLast_nameChange = (e) => {
        setLast_name(e.target.value)
    }

    const handleEmployee_idChange = (e) => {
        setEmployeeid(e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        console.log(data);
        const url = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFirst_name('');
            setLast_name('');
            setEmployeeid('');
        }
    }

    // useEffect(() => {
    //     fetchData();
    // }, [])


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirst_nameChange} placeholder="firstName" required type="text" name="firstName" id="firstName" className="form-control" value={first_name} />
                            <label htmlFor="firstName">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLast_nameChange} placeholder="lastName" required type="text" name="lastName" id="lastName" className="form-control" value={last_name} />
                            <label htmlFor="lastName">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployee_idChange} placeholder="employeeId" required type="text" name="employeeId" id="employeeId" className="form-control" value={employee_id} />
                            <label htmlFor="employeeId">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )

}
export default TechnicianForm
