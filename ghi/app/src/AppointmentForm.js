import React, { useEffect, useState } from 'react';


function AppointmentForm() {
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);

    const handleDateChange = (e) => {
        setDate(e.target.value)
    }

    const handleTimeChange = (e) => {
        setTime(e.target.value)
    }

    const handleReasonChange = (e) => {
        setReason(e.target.value)
    }


    const handleVinChange = (e) => {
        setVin(e.target.value)
    }

    const handleCustomerChange = (e) => {
        setCustomer(e.target.value)
    }

    const handleTechnicianChange = (e) => {
        setTechnician(e.target.value)
    }


    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.date_time = `${date}:${time}`;
        data.reason = reason;
        data.vin = vin;
        data.customer = customer;
        data.technician = technician;

        const url = 'http://localhost:8080/api/appointments/'
        console.log(data)
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setDate('');
            setTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
        }
    };

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Schedule an Appointment</h1>
                    <form id="create-appointment-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} placeholder="Vin" value={vin} required type="text" id="vin" className="form-control" name="vin" />
                            <label htmlFor="model">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} placeholder="Customer" value={customer} required type="text" id="customer" className="form-control" name="customer" />
                            <label htmlFor="model">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateChange} placeholder="Date" value={date} required type="date" id="date" className="form-control" name="date" />
                            <label htmlFor="Date">Date</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="Time">Time</label>
                            <input onChange={handleTimeChange} required type="time" value={time} id="time" className="form-control" name="time" />
                        </div>
                        <div className="mb-3">
                            <select onChange={handleTechnicianChange} requiredtype="" id="technician" name="employee_id" className="form-select" value={technician}>
                                <option value="">Choose a technician</option>
                                {technicians.map(technician => (
                                    <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                                ))}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} placeholder="reason" value={reason} required type="text" id="reason" className="form-control" name="reason" />
                            <label htmlFor="model">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default AppointmentForm;
